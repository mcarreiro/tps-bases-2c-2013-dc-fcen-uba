\begin{verbatim}

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bases_uba_tp1`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `validate_inscription`(IN `idSocioS` INT, IN `idCronogramaS` INT, IN `idBaseS` INT)
    READS SQL DATA
_main: BEGIN

DECLARE CATEGORIA_SIN_PERMISO CONDITION FOR SQLSTATE '45000';
DECLARE id_actividad_del_cronograma INT;
DECLARE categoria_minima INT;
DECLARE id_categoria_socio INT;

SELECT idActividad INTO id_actividad_del_cronograma FROM cronograma WHERE idCronograma = idCronogramaS;
SELECT idCategoria INTO categoria_minima FROM puede_hacer_actividad WHERE idActividad = id_actividad_del_cronograma;
SELECT categoria INTO id_categoria_socio FROM socio where idSocio = idSocioS AND pertenece_a = idBaseS;

if id_categoria_socio != categoria_minima THEN
	SIGNAL CATEGORIA_SIN_PERMISO
	SET MESSAGE_TEXT = 'El socio no posee la categoria minima necesaria';
END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `validate_profesor_correcto_en_cronograma`(IN `idProfesorS` INT, IN `idActividadS` INT)
    READS SQL DATA
_main: BEGIN

DECLARE PROFESOR_INCORRECTO CONDITION FOR SQLSTATE '55000';

DECLARE cantidad_de_profesores INT;

SELECT count(1) 
INTO cantidad_de_profesores 
FROM a_cargo 
WHERE idActividad = idActividadS
AND idProfesional = idProfesorS;

if cantidad_de_profesores = 0 THEN
	SIGNAL PROFESOR_INCORRECTO
	SET MESSAGE_TEXT = 'El profesor no da esa actividad';
END IF;


END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `actividad`
--

CREATE TABLE IF NOT EXISTS `actividad` (
  `idActividad` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `arancel` int(11) NOT NULL,
  PRIMARY KEY (`idActividad`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `actividad`
--

INSERT INTO `actividad` (`idActividad`, `nombre`, `arancel`) VALUES
(1, 'Básket', 0),
(2, 'Futbol', 0),
(3, 'Tenis', 40),
(4, 'Voley', 10),
(5, 'Ping Pong', 0),
(6, 'Cricket', 0),
(7, 'Rugby', 50),
(8, 'Hockey', 10);

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE IF NOT EXISTS `area` (
  `idArea` int(11) NOT NULL AUTO_INCREMENT,
  `estado` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `capacidad` int(11) NOT NULL,
  `ubicacion` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`idArea`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`idArea`, `estado`, `capacidad`, `ubicacion`) VALUES
(1, 'a', 1, '1'),
(2, 'Con detalles de uso', 10000, 'Pabellon 2'),
(3, 'Olor irrespirable y mucho frio', 2, 'Bombonera'),
(4, 'Excelente', 100000, 'Monumental'),
(5, 'Poco', 11, 'Parque Norte');

-- --------------------------------------------------------

--
-- Table structure for table `a_cargo`
--

CREATE TABLE IF NOT EXISTS `a_cargo` (
  `idActividad` int(11) NOT NULL,
  `idProfesional` int(11) NOT NULL,
  PRIMARY KEY (`idActividad`,`idProfesional`),
  KEY `idProfesional` (`idProfesional`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `a_cargo`
--

INSERT INTO `a_cargo` (`idActividad`, `idProfesional`) VALUES
(1, 1),
(2, 2),
(6, 2),
(3, 3),
(6, 3),
(1, 4),
(4, 4),
(6, 5),
(3, 7);

-- --------------------------------------------------------

--
-- Table structure for table `categoria`
--

CREATE TABLE IF NOT EXISTS `categoria` (
  `idCategoria` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `porcentajeCuotaSocial` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`idCategoria`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `categoria`
--

INSERT INTO `categoria` (`idCategoria`, `descripcion`, `porcentajeCuotaSocial`, `nombre`) VALUES
(1, '', 1, 'vitalicios'),
(2, '', 1, 'mayores'),
(3, '', 10, 'Infantiles');

-- --------------------------------------------------------

--
-- Table structure for table `cronograma`
--

CREATE TABLE IF NOT EXISTS `cronograma` (
  `turno` int(11) NOT NULL,
  `periodo` int(11) NOT NULL,
  `idActividad` int(11) NOT NULL,
  `idCronograma` int(11) NOT NULL AUTO_INCREMENT,
  `idProfesosional` int(11) NOT NULL,
  PRIMARY KEY (`idCronograma`),
  KEY `idActividad` (`idActividad`),
  KEY `idProfesor` (`idProfesosional`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `cronograma`
--

INSERT INTO `cronograma` (`turno`, `periodo`, `idActividad`, `idCronograma`, `idProfesosional`) VALUES
(1, 2012, 1, 1, 1),
(1, 2012, 2, 2, 2),
(1, 2012, 4, 6, 4);

--
-- Triggers `cronograma`
--
DROP TRIGGER IF EXISTS `validate_profesor_correcto_en_cronograma`;
DELIMITER //
CREATE TRIGGER `validate_profesor_correcto_en_cronograma` BEFORE INSERT ON `cronograma`
 FOR EACH ROW BEGIN
    CALL validate_profesor_correcto_en_cronograma(
        NEW.idProfesosional
    ,	NEW.idActividad
    );
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `cuota_social`
--

CREATE TABLE IF NOT EXISTS `cuota_social` (
  `idCuotaSocial` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `monto_a_pagar` int(11) NOT NULL,
  `monto_abonado` int(11) DEFAULT NULL,
  `deuda_acumulada` int(11) NOT NULL,
  `periodo` int(11) NOT NULL,
  `grupo_familiar` int(11) NOT NULL,
  PRIMARY KEY (`idCuotaSocial`),
  KEY `periodo` (`periodo`),
  KEY `grupo_familiar` (`grupo_familiar`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `cuota_social`
--

INSERT INTO `cuota_social` (`idCuotaSocial`, `fecha`, `monto_a_pagar`, `monto_abonado`, `deuda_acumulada`, `periodo`, `grupo_familiar`) VALUES
(1, '2010-01-05', 100, 10, 90, 2013, 1),
(5, '2010-03-05', 100, 100, 0, 1, 2),
(6, '2010-05-05', 100, 110, 0, 1, 3),
(7, '2010-07-05', 100, 100, 0, 1, 4),
(8, '2010-09-05', 100, 110, 0, 1, 5),
(9, '2013-09-09', 120, 100, 20, 2013, 2);

-- --------------------------------------------------------

--
-- Table structure for table `cuota_social_valor`
--

CREATE TABLE IF NOT EXISTS `cuota_social_valor` (
  `monto_base` int(11) NOT NULL,
  `periodo` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`periodo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=2014 ;

--
-- Dumping data for table `cuota_social_valor`
--

INSERT INTO `cuota_social_valor` (`monto_base`, `periodo`) VALUES
(1, 1),
(1, 2),
(3, 3),
(1, 2011),
(2, 2013);

-- --------------------------------------------------------

--
-- Table structure for table `dado_por`
--

CREATE TABLE IF NOT EXISTS `dado_por` (
  `idCronograma` int(11) NOT NULL,
  `idProfesional` int(11) NOT NULL,
  PRIMARY KEY (`idCronograma`,`idProfesional`),
  KEY `idProfesional` (`idProfesional`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `dado_por`
--

INSERT INTO `dado_por` (`idCronograma`, `idProfesional`) VALUES
(1, 3),
(2, 6);

-- --------------------------------------------------------

--
-- Table structure for table `grupo_familiar`
--

CREATE TABLE IF NOT EXISTS `grupo_familiar` (
  `idBase` int(50) NOT NULL AUTO_INCREMENT,
  `domicilio` varchar(1000) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` int(100) NOT NULL,
  PRIMARY KEY (`idBase`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `grupo_familiar`
--

INSERT INTO `grupo_familiar` (`idBase`, `domicilio`, `telefono`) VALUES
(1, 'Bulnes 1900', 48235678),
(2, 'Av Cabildo 2524', 48290909),
(3, 'Santa Fe 1234', 48250993),
(4, 'Cordoba 324', 43445900),
(5, 'Jujuy 444', 32440944),
(6, 'Ortiz de Ocampo 123', 48224532);

-- --------------------------------------------------------

--
-- Table structure for table `pago`
--

CREATE TABLE IF NOT EXISTS `pago` (
  `idPago` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `monto` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `calculo_deuda` int(11) NOT NULL,
  `idSocio` int(11) NOT NULL,
  `idCronograma` int(11) NOT NULL,
  `idBase` int(11) NOT NULL,
  PRIMARY KEY (`idPago`),
  KEY `idBase` (`idBase`),
  KEY `idSocio` (`idSocio`),
  KEY `idCronograma` (`idCronograma`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `pago`
--

INSERT INTO `pago` (`idPago`, `tipo`, `monto`, `fecha`, `calculo_deuda`, `idSocio`, `idCronograma`, `idBase`) VALUES
(5, 'bimestral', 20, '2013-09-03', 0, 0, 2, 4),
(6, 'semanal', 33, '2013-09-11', 0, 1, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `profesional`
--

CREATE TABLE IF NOT EXISTS `profesional` (
  `idProfesional` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_apellido` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `dni` int(11) NOT NULL,
  `tel` int(11) NOT NULL,
  PRIMARY KEY (`idProfesional`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `profesional`
--

INSERT INTO `profesional` (`idProfesional`, `nombre_apellido`, `dni`, `tel`) VALUES
(1, 'Jonny Tolengo', 24958675, 45796363),
(2, 'Ricardo Rivas', 17645259, 45136699),
(3, 'Henry Ford', 33146485, 48965252),
(4, 'Pedro Rodríguez', 32125489, 45256978),
(5, 'Chun Li', 29643741, 43259494),
(6, 'Juan Sebastian Veron', 24512963, 46987542),
(7, 'Diego Armando Maradona', 14569325, 48569578),
(8, 'Ariel Ortega', 27659456, 42596384);

-- --------------------------------------------------------

--
-- Table structure for table `puede_hacer_actividad`
--

CREATE TABLE IF NOT EXISTS `puede_hacer_actividad` (
  `idCategoria` int(11) NOT NULL,
  `idActividad` int(11) NOT NULL,
  PRIMARY KEY (`idCategoria`,`idActividad`),
  KEY `idActividad` (`idActividad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `puede_hacer_actividad`
--

INSERT INTO `puede_hacer_actividad` (`idCategoria`, `idActividad`) VALUES
(1, 1),
(2, 2),
(3, 3),
(1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `se_desarrolla_en`
--

CREATE TABLE IF NOT EXISTS `se_desarrolla_en` (
  `idActividad` int(11) NOT NULL,
  `idArea` int(11) NOT NULL,
  PRIMARY KEY (`idActividad`,`idArea`),
  KEY `idArea` (`idArea`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `se_desarrolla_en`
--

INSERT INTO `se_desarrolla_en` (`idActividad`, `idArea`) VALUES
(1, 1),
(1, 2),
(2, 3),
(2, 4),
(3, 4),
(2, 5);

-- --------------------------------------------------------

--
-- Table structure for table `se_inscribe_en`
--

CREATE TABLE IF NOT EXISTS `se_inscribe_en` (
  `idSocio` int(11) NOT NULL,
  `idCronograma` int(11) NOT NULL,
  `idBase` int(11) NOT NULL,
  PRIMARY KEY (`idSocio`,`idCronograma`,`idBase`),
  KEY `idBase` (`idBase`),
  KEY `idCronograma` (`idCronograma`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `se_inscribe_en`
--

INSERT INTO `se_inscribe_en` (`idSocio`, `idCronograma`, `idBase`) VALUES
(1, 2, 1),
(1, 2, 2),
(0, 6, 4),
(2, 1, 4);

--
-- Triggers `se_inscribe_en`
--
DROP TRIGGER IF EXISTS `validate_categoria`;
DELIMITER //
CREATE TRIGGER `validate_categoria` BEFORE INSERT ON `se_inscribe_en`
 FOR EACH ROW BEGIN
    CALL validate_inscription(
        NEW.idSocio
    ,   NEW.idCronograma
    ,   NEW.idBase
    );
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `socio`
--

CREATE TABLE IF NOT EXISTS `socio` (
  `idSocio` int(11) NOT NULL,
  `nombre` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `pertenece_a` int(11) NOT NULL,
  `categoria` int(11) NOT NULL,
  PRIMARY KEY (`idSocio`,`pertenece_a`),
  KEY `pertenece_a` (`pertenece_a`),
  KEY `categoria` (`categoria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `socio`
--

INSERT INTO `socio` (`idSocio`, `nombre`, `email`, `fecha_nacimiento`, `pertenece_a`, `categoria`) VALUES
(0, 'Paula Ramírez', '', '1985-04-17', 4, 1),
(1, 'Carlos Allen', 'callen@mail.com', '1970-05-03', 1, 2),
(1, 'Juan Cascos', 'jcascos@mail.com', '1980-03-02', 2, 2),
(1, 'Pedro Cascos', '', '2008-09-17', 3, 1),
(2, 'Santiago Allen', 'callen@mail.com', '2003-11-03', 1, 1),
(2, 'Lucía Allen', 'lallen@mail.com', '2005-09-02', 2, 1),
(2, 'Marco Cascos', 'mcascos@mail.com', '2002-03-02', 3, 3),
(3, 'Paula Allen', 'lallen@mail.com', '2005-09-02', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ubicada_en`
--

CREATE TABLE IF NOT EXISTS `ubicada_en` (
  `idCronograma` int(11) NOT NULL,
  `idArea` int(11) NOT NULL,
  PRIMARY KEY (`idCronograma`,`idArea`),
  KEY `idArea` (`idArea`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `ubicada_en`
--

INSERT INTO `ubicada_en` (`idCronograma`, `idArea`) VALUES
(6, 2),
(1, 3);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `a_cargo`
--
ALTER TABLE `a_cargo`
  ADD CONSTRAINT `a_cargo_ibfk_2` FOREIGN KEY (`idActividad`) REFERENCES `actividad` (`idActividad`) ON DELETE CASCADE,
  ADD CONSTRAINT `a_cargo_ibfk_1` FOREIGN KEY (`idProfesional`) REFERENCES `profesional` (`idProfesional`);

--
-- Constraints for table `cronograma`
--
ALTER TABLE `cronograma`
  ADD CONSTRAINT `cronograma_ibfk_2` FOREIGN KEY (`idProfesosional`) REFERENCES `profesional` (`idProfesional`),
  ADD CONSTRAINT `cronograma_ibfk_1` FOREIGN KEY (`idActividad`) REFERENCES `actividad` (`idActividad`);

--
-- Constraints for table `cuota_social`
--
ALTER TABLE `cuota_social`
  ADD CONSTRAINT `cuota_social_ibfk_2` FOREIGN KEY (`grupo_familiar`) REFERENCES `grupo_familiar` (`idBase`),
  ADD CONSTRAINT `cuota_social_ibfk_1` FOREIGN KEY (`periodo`) REFERENCES `cuota_social_valor` (`periodo`);

--
-- Constraints for table `dado_por`
--
ALTER TABLE `dado_por`
  ADD CONSTRAINT `dado_por_ibfk_2` FOREIGN KEY (`idProfesional`) REFERENCES `profesional` (`idProfesional`),
  ADD CONSTRAINT `dado_por_ibfk_1` FOREIGN KEY (`idCronograma`) REFERENCES `cronograma` (`idCronograma`);

--
-- Constraints for table `pago`
--
ALTER TABLE `pago`
  ADD CONSTRAINT `pago_ibfk_3` FOREIGN KEY (`idCronograma`) REFERENCES `cronograma` (`idCronograma`),
  ADD CONSTRAINT `pago_ibfk_1` FOREIGN KEY (`idBase`) REFERENCES `grupo_familiar` (`idBase`),
  ADD CONSTRAINT `pago_ibfk_2` FOREIGN KEY (`idSocio`) REFERENCES `socio` (`idSocio`);

--
-- Constraints for table `puede_hacer_actividad`
--
ALTER TABLE `puede_hacer_actividad`
  ADD CONSTRAINT `puede_hacer_actividad_ibfk_2` FOREIGN KEY (`idActividad`) REFERENCES `actividad` (`idActividad`),
  ADD CONSTRAINT `puede_hacer_actividad_ibfk_1` FOREIGN KEY (`idCategoria`) REFERENCES `categoria` (`idCategoria`);

--
-- Constraints for table `se_desarrolla_en`
--
ALTER TABLE `se_desarrolla_en`
  ADD CONSTRAINT `se_desarrolla_en_ibfk_2` FOREIGN KEY (`idArea`) REFERENCES `area` (`idArea`),
  ADD CONSTRAINT `se_desarrolla_en_ibfk_1` FOREIGN KEY (`idActividad`) REFERENCES `actividad` (`idActividad`);

--
-- Constraints for table `se_inscribe_en`
--
ALTER TABLE `se_inscribe_en`
  ADD CONSTRAINT `se_inscribe_en_ibfk_1` FOREIGN KEY (`idSocio`) REFERENCES `socio` (`idSocio`),
  ADD CONSTRAINT `se_inscribe_en_ibfk_2` FOREIGN KEY (`idCronograma`) REFERENCES `cronograma` (`idCronograma`),
  ADD CONSTRAINT `se_inscribe_en_ibfk_3` FOREIGN KEY (`idBase`) REFERENCES `grupo_familiar` (`idBase`);

--
-- Constraints for table `socio`
--
ALTER TABLE `socio`
  ADD CONSTRAINT `socio_ibfk_2` FOREIGN KEY (`categoria`) REFERENCES `categoria` (`idCategoria`),
  ADD CONSTRAINT `socio_ibfk_1` FOREIGN KEY (`pertenece_a`) REFERENCES `grupo_familiar` (`idBase`);

--
-- Constraints for table `ubicada_en`
--
ALTER TABLE `ubicada_en`
  ADD CONSTRAINT `ubicada_en_ibfk_2` FOREIGN KEY (`idArea`) REFERENCES `area` (`idArea`),
  ADD CONSTRAINT `ubicada_en_ibfk_1` FOREIGN KEY (`idCronograma`) REFERENCES `cronograma` (`idCronograma`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
\end{verbatim}